# DT-830, Reverse Engineering

![](img/Photo_2.jpg)
## Datasheet ICL7106

[Datasheet ICL7106 Renesas](img/REN_icl7106-07-07s_DST_20001128.pdf)
[Datasheet ICL7106 with battery Indicator HN-ELEC](img/1471420678.pdf)
## Signals

### Display segment

Frequency: 55.70Hz
43.69kHz/ 800 = 54,6125
55.70 x 800 = 44.560kHz
![](img/Segment.jpg)

### RC Clock

Frequency : 
- 40kHz (with oscilloscope probe on RC).
- 43.69kHz (probes on a square signal).
- **44.560kHz** (computed from segment frequency)

16000/43.69 = 366.44ms
16000/44.560 = 359,07ms

#RC_Clock

![](img/RC_osc.jpg)
![](img/Clock.jpg)

### Voltage Buffer

#Dual_Slope_ADC

![](img/Phases.jpg)
![](img/Buffer_States.jpg)

Vref = 100mV

The ratio (tRef / tSignal)*1000 is the displayed value on the digits.


### Comparison

4 x 1000 / 44.560 kHz = 89.87ms

t0   = 360 - tSignal - tRef

#Timings

| Timings | High Voltage | Low Voltage | High Voltage Counts | Low Voltage Counts |
| ------- | ------------ | ----------- | ------------------- | ------------------ |
| tAZ     | 208.0ms      | 240.0ms     | 2260.87             | 2608.70            |
| tRef    | 60ms         | 28ms        | 652.17              | 304.35             |
| tSignal | 92ms         | 92ms        | 1000.00             | 1000.00            |
| Total   | 360.0ms      | 360.0ms     | 3913.04             | 3913.04            |
#### High Voltage (LO / HI)

![tAZ](img/Buffer2.jpg)
![tSignal](img/Buffer3.jpg)
![tRef](img/Buffer1.jpg)

#### Low Voltage (LO / HI)
![tAZ](img/Buffer4.jpg)
![tSignal](img/Buffer5.jpg)
![tRef](img/Buffer6.jpg)

## Resistor values

| Component | Description  | Value   |
|-----------|--------------|---------|
| R6        | Oscillator   | 101kOhm |
| R7        | Integrator   | 100kOhm |
| R10       |              | 220kOhm |
| R11       | Input        | 1MOhm   |

![](img/R6.jpg)
![](img/R10.jpg)
![](img/R11.jpg)
![](img/R7.jpg)

![](img/IC_pinout.jpg)


![](img/Photo.jpg)

## Pinout

#Pinout #ICL7106

![](img/Display%20Segments.jpg)

![IC_Photo_Pinout](img/IC_Photo_Pinout.jpg)

### Unlabeled pins are the Low-BAT indicator and possibly hold

![](img/Hidden%20Pins.jpg)

Some datasheets mention a LB indicator, some indicates NC pin.

>  Display-hold, low-battery flag, integration and de-integration status flags are four additional features which are available in the 44-pin package

![](img/LowBat.jpg)

With a full battery, segment signals are in phase (no indicator)

![](img/Lowbat_2.jpg)

With a dead battery, segment signal are in phase opposition (low bat indicator)
![](img/Lowbat_1.jpg)

## Schematic
Source : [Peter Vis](img/petervis.com)

#Schematic

![](img/dt830b-circuit-diagram.gif)