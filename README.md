# Hacking the DT-830B

I bought a cheap (5€) multimeter at ACTION store I want to hack and create a USB logging multimeter.

![](img/DMM.jpg)

## The complete reverse engineering

The complete reverse engineering is in the [reverseEngineering.md](reverseEngineering.md) file.

## Modification for USB loggin using a comparator and a Arduino (WIP)

A log of my current progress in tinkering with this DMM is in the [logMods.md](mods/logMods.md) file.