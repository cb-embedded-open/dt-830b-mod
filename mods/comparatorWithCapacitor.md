# Dual comparator with serial capacitor

## Theory of operation

As a fix of the previous circuit [dual Comparator With Hysteresis](dualComparatorWithHysteresis.md), a capacitor is added to filter all dc component and only keep the edges of the waveform.

Suppose the output of the comparator is high and there is a -100mV falling edge at the input. So the input signal on the non-inverting input, even with the hysterisis, will see this falling edge and flip the output.

Next, the input signal will rise again to 0, so the non-inverting input will see a rising edge of +100mV and flip the output again. The next rising edges will be ignored, as the output is already high.

#LTSpice #Simulation #Series_Capacitor

![LTSpice positive](../img/LTspice_positive.png)
![LTSpice negative](../img/LTSpice_negative.png)
![LTSpice Extreme](../img/LTSpice_extreme.png)

## Observed waveforms

In the real world, the comparator output is chaotic. Big glitches are observed on the raw waveform. Even the oscilloscope can't correctly trigger.

### Large observed glitches

The oscilloscope see short (tens of nanosecond) pulses that cause a few µs ringing of the input. This ringing seems to be the cause of the glitches at the output of the comparator.

#Glitches

![Glitches](../img/Glitches_buffer.jpg)

#Buffer_wavefrom

![Glitches buffer waveform](../img/Buffer_Waveform.jpg)

### Circuit checked

Capacitor Added (I doubly checked connections with multimeter, there is solder on the bottom side too for connecting 10k resistor to common, and the other side to the capacitor)
![Capacitor Added](../img/CapacitorAdded.jpg)

## Suggested fix

See [Comparator with capacitor and filtering caps](comparatorWithCapacitorAndFilteringCaps.md)