# Modification for USB loggin using a comparator and a Arduino

## Main idea

The idea is to first convert the buffer waveform, composed of 3 levels (0, signal, opposite sign reference)
into a simple square wave, where the duty cycle times 4000 is the measured value in counts.

## Possible Implementations

* [Single 5V tied comparator with capacitor](singleComparatorWithCapacitor.md)
* [Single comparator, 9V tied, with a pull-down resistor](comparatorRefPullDown.md)
* [Dual comparator with hysteresis](dualComparatorWithHysteresis.md)
* [Dual comparator with hysteresis and series DC filtering capacitor](comparatorWithCapacitor.md)
* [Dual comparator with hysteresis and series DC filtering capacitor and filtering caps](comparatorWithCapacitorAndFilteringCaps.md)

## Summary

| Comparator Type   | Voltage | With Capacitor                                                                                                   | With Hysteresis                               | With Pull-Down Resistor                | With Filtering Caps |
| ----------------- | ------- | ---------------------------------------------------------------------------------------------------------------- | --------------------------------------------- | -------------------------------------- | ------------------- |
| Single Comparator | 5V      | Yes ([Link](singleComparatorWithCapacitor.md))                                                                   | No                                            | No                                     | No                  |
| Single Comparator | 9V      | No                                                                                                               | No                                            | Yes ([Link](comparatorRefPullDown.md)) | No                  |
| Dual Comparator   | 5V / 9V       | No                                                                                                               | Yes ([Link](dualComparatorWithHysteresis.md)) | No                                     | No                  |
| Dual Comparator   | 5V / 9V       | Yes, with series DC filtering ([Link](comparatorWithCapacitor.md))                                               | Yes                                           | No                                     | No                  |
| Dual Comparator   | 5V / 9V       | Yes, with series DC filtering and additional filtering caps ([Link](comparatorWithCapacitorAndFilteringCaps.md)) | Yes                                           | No                                     | Yes                 |

