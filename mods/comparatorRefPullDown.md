# Comparator with lower than common threshold

## Theory of operation

A 1M resistor pull the inverting input slightly lower than the zero common level.

*Note : this threshold only works well for positive voltages. If the integrated voltage is too close to common, it will be below the threshold. Hysteresis should be implemented correctly.*

## Schematic

![](../img/Comparator%20Schematic.jpg)

## Conclusion

Circuit not implemented, cause does not work well with negative voltages.