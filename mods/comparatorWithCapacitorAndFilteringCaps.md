# Comparator with capacitor and filtering caps

## Theory of operation

As a fix of the previous circuit [Comparator with series capacitor](comparatorWithCapacitor.md), filtering caps are added to the comparator to remove the glitches:

* add filtering caps between inputs of the comparator.
* add series resistor to form a low-pass filter with the input capacitance.
* add decoupling capacitors in the real-world circuit.
### Simulation EveryCircuit

I have simulated with noise, with and without the fix.
In the simulation, I added a resistor in series and a 100pF capacitor between 100pF to form a RC low-pass filter designed to filter out the glitches.

#EveryCircuit 

https://everycircuit.com/circuit/5417045478604800

![](../img/Every%20circuit%20Fix%20RC%20filter.jpg)

We need to check that the filter will not affect too much the edges of the original waveform. The effect should be less than a count to keep the reading consistent with the multimeter display.