# Comparator with capacitor for removing DC

## Theory of operation

In this initial idea, we use a comparator powered with 5V psu (like the internal analog comparator of the Arduino) and we use capacitor to remove the high DC offset from the multimeter (powered with 9V battery).

#EveryCircuit

https://everycircuit.com/circuit/5417045478604800
![DT-830 Measure](../img/DT-830%20Measure.png)