# Dual Comparator:  hysteresis + 5V output for Arduino

## Theory of operation

Comparator with hysteresis generate an output voltage between 0V and 9V with a 4.7k pull-up resistor on the open-collector output of the LM393.

The second comparator is used to generate a 5V output for the Arduino, using the internal pull-up resistor of the Arduino.

## Schematic

![](../img/Dual%20Comparator%20hysteresis%205V%20out.jpg)

## Prototype

## Wiring on protoboard

![](../img/Wiring%20CMP.jpg)
![](../img/Wiring%20labels.jpg)
![](../img/Wiring%20Soldering.jpg)

#Prototype #Wiring

## Connection to multimeter

![](../img/Jumpwires.jpg)


## Experiment results

8.82 V measured (from a dead 9V battery)

![](../img/Measured%20voltage.jpg)

### Measured Waveforms

Issue observed :
* t_on = 90ms (~1006 counts) so, integrating time instead of de-integrating ?
* t_period = 358ms (4000 counts)
* Glitches (bad hysteresis ?)

![](../img/Comp%20T_on.jpg)
![](../img/Comp%20total.jpg)
![](../img/Comp%20glitches.jpg)

#Glitches 

## Simulation

Complementary LT-Spice simulation (not shown here) indicates that the trigger is not working as expected : the hysteresis does not latch on the low level, but on the high level.

#Simulation 
## Conclusion

The circuit proposed is not suitable for the application. The hysteresis is not working as expected. The noise induces unwanted glitches. The circuit is not reliable and the duty cycle is static and does not reflect the input voltage.